(defvar emacs-bin "emacs")
(defvar publisher 'el-publisher)
(defvar exporter 'org-v9)
(defvar theme
  'readtheorg
  ;; 'experiment
  ;; 'dev
)


(defvar math-theme 'math)
(provide 'prj-config)

(defvar context
  '(popl
	readtheorg
	math))
	
