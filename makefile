SHELL:=/bin/bash
home=$(shell echo $$HOME)
prj-dir=$(shell pwd -P)
# Custom parameters
# =================
# res: directory for reusble resources,
# including exporters, emacs-utils, publisher,
# catalog etc.
# must be a fully qualified directory without trailing slash
res=/tmp/org-infra
force=
# force=true

utils-dir=${res}/utils
publishers-dir=${res}/publishers

# command to run emacs, emacs 25.x needed
# CUSTOMIZABLE
emacs=emacs

# symlink in src linking to to exporter directory
exp=exp
exp-dir=${prj-dir}/${exp}

# Common Directories
# -------------------
build-dir=${prj-dir}/build
build-docs-dir=${build-dir}/docs
src-dir=${prj-dir}/src

# Repositories and directories
# ----------------------------
emacs-utils-repo=https://gitlab.com/vxcg/pub/emacs/emacs-utils.git
emacs-utils-dir=${utils-dir}/emacs-utils

publisher-repo=https://gitlab.com/vxcg/pub/orgmode/el-publisher.git
publisher-dir=${publishers-dir}/el-publisher

# printing reveal slides using decktape
# -------------------------------------
# assumes (node and) decktape are installed
# -----------------------------------------
decktape=~/apps/node_modules/.bin/decktape

top: slides

all: publish

init:
	(mkdir -p ${res} ${build-docs-dir} ${publishers-dir} ${utils-dir})

utils: init
	(source ./init.sh; git-install ${emacs-utils-dir} ${emacs-utils-repo})

publisher: utils
	(source ./init.sh; git-install ${publisher-dir} ${publisher-repo})

resources: publisher
	(cd ${publisher-dir} && make -k install-resources prj-dir=${prj-dir} res=${res})

packages: resources
	(${emacs} -q --script ${exp-dir}/elisp/package-install-cli.el  \
	"${emacs-utils-dir}:${exp-dir}/elisp" ${exp-dir})

themes: init
	(rsync -a ${src-dir}/themes/ ${build-dir}/docs/themes)

publish: packages themes
	(${emacs} -q --script ${exp-dir}/elisp/publish-cli-proxy.el \
	"${emacs-utils-dir}:${exp-dir}/elisp" ${prj-dir} ${force})

pub: 
	(${emacs} -q --script ${exp-dir}/elisp/publish-cli-proxy.el \
	"${emacs-utils-dir}:${exp-dir}/elisp" ${prj-dir} ${force})

clean:
	(\rm -rf ${build-dir} ${exp-dir} ${src-dir}/themes ${src-dir}/plugin)

# the next three targets have to do with org-reveal slides
plugin:
	(mkdir -p ${src-dir}/plugin; cd ${src-dir}/plugin; \
    ln -sf ../themes/org-reveal/Reveal.js-TOC-Progress/plugin/toc-progress)

slides: plugin themes
	(${emacs} -q --script ${exp-dir}/elisp/publish-cli-proxy.el \
	"${emacs-utils-dir}:${exp-dir}/elisp" ${prj-dir} org-reveal)

# assumes decktape is installed
pdf:
	@echo "conv to pdf using decktape"
	${decktape} -s 1280x760 reveal src/index.html src/index.pdf

# uncomment this if you wish to add more make commands in extra.mk
# include extra.mk


